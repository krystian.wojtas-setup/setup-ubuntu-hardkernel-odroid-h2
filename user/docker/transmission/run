#!/bin/sh

# Stop on first failure
set -e

# Show each command
set -x

# Exit success if container is already created
if docker ps --all | grep --silent transmission; then
    echo >&2 "INFO: transmission container is already created"
    exit 0
fi

# Run docker container
docker \
    run \
        `# Set name of newly created container` \
        --name transmission \
        \
        `# Set container process user and group id` \
        --env PUID=`id --user www-data` \
        --env PGID=`id --group www-data` \
        \
        `# Specify a timezone to use Warsaw` \
        --env TZ=Europe/Warsaw \
        \
        `# Optionaly specify an alternative UI` \
        --env TRANSMISSION_WEB_HOME=/combustion-release/ \
        \
        `# Expose web ui` \
        --publish 9091:9091 \
        \
        `# Expose torrent tcp port` \
        --publish 51413:51413 \
        \
        `# Expose torrent udp port` \
        --publish 51413:51413/udp \
        \
        `# Transmission configs and logs` \
        --volume /media/storage/docker/transmission/config:/config \
        \
        `# Local path for downloads` \
        --volume /media/storage/files/torrents:/downloads \
        \
        `# Watch folder for torrent files` \
        --volume /media/storage/files/torrents/watch:/watch \
        \
        `# Always restart container, except that it is stopped manually` \
        `# Then even when docker deamon restarts it will not start this container` \
        --restart unless-stopped \
        \
        `# Run in background` \
        --detach \
        \
        `# Image to run` \
        linuxserver/transmission
