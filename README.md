# Purpose

Provision hardkernel odroid h2 board with software based on ubuntu 20.04 server.

# Features

- openvpn: enter local network from outsidde
- wireguard: separate vpn network without access to local network
- nextcloud: share private files
- grafana: visualise system load and sensors for weather
- etherpad: share docs
- kerberos: surveillance cameras.
- gitlab: web gui for code repositories
- transmission: share files by torrent

# Usage

Install basic software and configure system
``` sh
./install-system
```

Reboot to run newest kernel and apply `docker` group for current user. Also to have `password store`.

Optionally configure `password store` like in [tutorial](https://medium.com/@chasinglogic/the-definitive-guide-to-password-store-c337a8f023a1)
``` sh
gpg --full-gen-key
pass init
```

Optionally provide configuration with secrets for wireguard like [example](https://gitlab.com/krystian.wojtas-setup/setup-common/-/blob/master/user/local/wireguard-home/example/wg-server.conf)
``` sh
pass edit common/vpn/home/wireguard/server/wg-server.conf
```
Wireguard would need running script again to start service
``` sh
./install-system
```

Optionally provide configuration with secrets for nextcloud service
``` sh
pass edit host/nextcloud/config
```

Install user configuration

``` sh
./install-user
```
