#!/bin/bash

# Manuall steps required once before first initialization run from host
#   * exchange ssh key
#       ssh-copy-id k@h2
#   * Download this repository
#       ssh h2 'git clone https://gitlab.com/krystian.wojtas/setup-ubuntu-hardkernel-odroid-h2.git'
#   * initialize password store
#       rsync -av --delete "$HOME/.password-store/h2" h2:.password-store/host
#       gpg --export-secret-keys krystian/h2 | ssh h2 'gpg --import'
#
# Then run this script
#       ssh -t h2 'cd setup-ubuntu-hardkernel-odroid-h2 ; git pull ; sudo ./initialize'

# Run script

# Stop on first failure
set -o errexit

# Pipe fail if any its command fails
set -o pipefail

# Exit if undeclared variable is read
set -o nounset

# Show each command
set -o xtrace

# Go to script directory
cd "$(realpath "$(dirname "$0")")"

# Set locale settings
update-locale LANGUAGE=en_US LC_ALL=en_US.UTF-8

# Allow incoming ssh connections
ufw allow ssh

# Limit ssh login attempts
ufw limit ssh

# Allow incoming wireguard connections
ufw allow 40000/udp

# Allow incoming openvpn connections
ufw allow 1194/udp

# Allow incoming wireguard connections
ufw allow 1195/udp

# Allow traffic to prometheus
ufw allow 9090/tcp

# Allow traffic to webthings
ufw allow 8080/tcp

# Allow traffic to portainer
ufw allow 9443/tcp

# Allow traffic to nfs
# nfs 2049
ufw allow from 192.168.1.0/24 to any port nfs
# rpcbind on configured static port
ufw allow from 192.168.1.0/24 to any port 33333
# portmapper
ufw allow from 192.168.1.0/24 to any port 111

# Enable firewall even from ssh connection
ufw --force enable

# Update ubuntu software repository
apt-get \
    update \

# Install some extra software packages
apt-get \
    install \
        --yes \
        \
        cockpit \
        cryptsetup \
        docker.io \
        easy-rsa \
        emacs-nox \
        lm-sensors \
        lsof \
        mosh \
        net-tools \
        nfs-kernel-server \
        nmap \
        openvpn \
        pass \
        picocom \
        tree \
        wireguard \
        xfsprogs \
        zsh \

# Install common system files
\
`# Download newest common archive` \
wget  \
    \
    `# Print downloaded file to standard output` \
    --output-document \
        - \
    https://gitlab.com/krystian.wojtas-setup/setup-common/-/archive/master/setup-common-master.tar \
|
`# Unpack archive and overwrite root directory` \
tar \
    --extract \
    \
    `# Unpack to root directory` \
    --directory \
        / \
    \
    `# Do not change permissions mode of any existing directories` \
    --no-overwrite-dir \
    \
    `# Ommit two directories levels with repository name and system sub directory` \
    --strip-components=2 \
    \
    `# Filter content to extract` \
    setup-common-master/system \

# Install common configuration files if exist
if test -d ./system/; then
    cp \
        \
        `# Copy directories recursively` \
        --recursive \
        \
        `# Never follow symbolic links in SOURCE` \
        --no-dereference \
        \
        `# Source directory` \
        "./system/." \
        \
        `# Target directory is filesystem root to overwrite` \
        / \

fi

# Set zsh as default shell for target user
chsh --shell /bin/zsh "$SUDO_USER"

# Add user to groups
usermod --append --groups www-data "$SUDO_USER"
# WARNING: Be aware it is dangerous
# WARNING: User in docker group must be consider as being root
# https://www.zopyx.com/andreas-jung/contents/on-docker-security-docker-group-considered-harmful
usermod --append --groups docker "$SUDO_USER"

# Create directory for docker containers data
mkdir --parent /media/storage/docker/

# Set current user as owner of docker containers data
chown "$SUDO_USER":"$SUDO_USER" /media/storage/docker/

# Generate certificates if not yet generated on host
./bin/openvpn-keys-generate

# Install generated certificates to system location if not yet done
./bin/openvpn-keys-install

# Enable service on startup and run it now
systemctl enable --now openvpn@server

# Enable service on startup and run it now
systemctl enable --now docker

# Export all nfs shares
exportfs -a

# Restart service to apply new changes
systemctl reload nfs-kernel-server

# Check if wireguard configuration with secrets is provided
# Example server config is in:
# - setup-common/user/local/wireguard-home/example/wg-server.conf
if secret="$(pass show common/vpn/home/wireguard/server/wg-server.conf)"; then

    # Install wireguard configuration
    echo "$secret" > /etc/wireguard/wg-server.conf

    # Enable service on startup and run it now
    systemctl enable --now wg-quick@wg-server

fi
